'''
PylintMultipleFiles v0.1: Uses Pylint to analyze multiple .py files.
Outputs the results as filename.py.pylint.txt files in the same directory.

To use: Put the .py files to analyze in the same directory as PylintMultipleFiles.py.
Then just run PylintMultipleFiles.py using Python.

Tested against: Pylint version 1.5.2 (Ubuntu GNU+Linux) and 1.6.5 (MS Win10)
Using Python version 3.5.2 (Ubuntu GNU+Linux) and 3.6.1 (MS Win10)

****************************************************************************************
Copyright (C) 2017 by William Paul Liggett (junktext@junktext.com)

This program is free/libre and open source software. You can redistribute
it or modify it under the terms of the license as specified below.

License: BSD 3-Clause
https://opensource.org/licenses/BSD-3-Clause

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other materials
provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be
used to endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
****************************************************************************************
'''

# `os' is the preferred cross-platform solution to talk to various OS platforms using Python.
import os

# FYI: Pylint wants any variable outside of a function/class to be in UPPERCASE.
# So, this is why all variables are named this way in this program.

# Runs Pylint in the shell as... PYLINT_CMD filename.py
# Therefore, you can set Pylint options here, such as: pylint --optimize-ast=y
PYLINT_CMD = "pylint"

# Python List[] of the current directory's files (including sub-directory names)
DIR_FILES = os.listdir()

# Tracks how many .py files were analyzed by Pylint.
PYLINT_REPORT_COUNT = 0

print("----------------------------------------------------------------------------\n"
      " PylintMultipleFiles: Uses Pylint to analyze multiple .py files.\n"
      " Outputs the results as filename.py.pylint.txt files in the same directory.\n"
      "----------------------------------------------------------------------------\n")

# Uses a for-loop to only analyze ".py" files in the directory.
for i in DIR_FILES:

    # Looks at the file extension to ensure it is a Python (.py) file.
    # Though, since a Python file might have an uppercase letter (.Py),
    # we temporarily force it to be in lowercase for evaluation.
    if i.lower()[-3:] == ".py":
        # Makes the report name to be: filename.py.pylint.txt
        PYLINT_REPORT_NAME = i + ".pylint.txt"

        # Deletes any old filename.py.pylint.txt reports that match this filename.
        if os.path.exists(PYLINT_REPORT_NAME):
            print("*** Deleting old Pylint report:", PYLINT_REPORT_NAME)
            os.remove(PYLINT_REPORT_NAME)

        # Below is how you'd assume to output all of the .txt files.
        # But, Pylint's file output names are really long and per-file stats are removed.
        #os.system("pylint --output-format=text --files-output=y " + i)

        # Instead, we'll force the .txt output we'd expect if we analyzed each file individually
        # PYLINT_CMD filename.py > filename.py.pylint.txt
        print("*** Creating new Pylint report:", PYLINT_REPORT_NAME, "\n\n")
        os.system(PYLINT_CMD + " " + i + " > " + PYLINT_REPORT_NAME)

        # Counts how many reports have been generated so far.
        PYLINT_REPORT_COUNT += 1

# Finishes by saying how many new Pylint reports were created.
print("*** TOTAL new Pylint reports created:", str(PYLINT_REPORT_COUNT))
print("*** Located at:", os.getcwd())
